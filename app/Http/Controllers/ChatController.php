<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inbox;

class ChatController extends Controller
{
    public function sendChat(Request $request){
        $request['status'] = "0";
        Inbox::insert($request->all());
        
        // Outbox::insert($request->all());
        return $request;
    }
}
